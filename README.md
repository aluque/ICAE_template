# LaTeX template for the International Conference on Atmospheric Electricity, Nara, Japan (2018).

This is a latex template that mostly follows the guidelines for full papers
for the International Conference on Atmospheric Electricity (ICAE), Nara,
Japan (2018). 

For more info and for paper submission visit [the ICAE 2018 website](http://icae2018.saej.jp).

